const mongoose = require('mongoose');
const config = require('./config.json');

function connection(){
    mongoose.connect(config.MongoURI).then(()=>{
        console.log("Successfully Connected with MongoDb Database");
    }).catch((err)=>{
        console.error("Error in Connecting Db",err);
        process.exit();
    });
}


module.exports = {connect:connection};