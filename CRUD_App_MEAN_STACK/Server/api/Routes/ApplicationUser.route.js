const express = require('express');
const router = express.Router();

const AppUser_Controller = require('../Controllers/ApplicationUser.Controller');

router.post('/login',AppUser_Controller.signin);
router.post('/signup_Login',AppUser_Controller.signup);

module.exports = router;