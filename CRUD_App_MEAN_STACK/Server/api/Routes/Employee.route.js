const express = require('express');
const router = express.Router();

const controller = require('../Controllers/Employee.Controller');

router.post('/addEmployee',controller.AddEmployee);
router.get('/getEmployees',controller.GetEmployees);
router.get('/getEmployeeById/:empId',controller.getEmployeeById);
router.put('/updateEmployee/:empId',controller.updateEmployee);
router.delete('/deleteEmployee/:empId',controller.deleteEmployee);

module.exports = router;