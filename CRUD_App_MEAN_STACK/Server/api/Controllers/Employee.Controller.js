const mongoose = require('mongoose');
require('../Models/Employee.Model');
const Employee = mongoose.model('Employees_Data');


module.exports.AddEmployee = async(req,res)=>{

    const {firstName,lastName,email,location,designation,phone} = req.body;

    try{
        const existingEmp = await Employee.findOne({email});
        if(existingEmp){
            return res.status(400).json({error:'The Given email details already exist in system. Try different email'});
        }
        const newEmployee = new Employee({firstName,lastName,email,location,designation,phone});
        await newEmployee.save();
        return res.status(201).json({info:`${firstName} ${lastName} Details Added Successfully`});
    }
    catch(err){
       return res.status(500).json({error:`Error in Adding new Employee Record - ${err}`});
    }

}


module.exports.GetEmployees = async(req,res)=>{
    try{
        const EmployeesList = await Employee.find();
        if(EmployeesList.length ===0){
            return res.status(200).json({info:'There is no Employees Data'});
        }
       return res.json(EmployeesList);

    }
    catch(err){
        return res.status(500).json({error:`Error in getting Employees List - ${err}`});
    }
}


module.exports.getEmployeeById = async(req,res)=>{
    const id = req.params.empId;
    try{
        const employee = await Employee.findById(id);
        return res.status(200).json(employee);
    }
    catch(err){
       return res.status(404).json({info:"Employee details not exists in this ID"});
    }

}

module.exports.updateEmployee = async(req,res)=>{

    const {firstName,lastName,email,location,designation,phone} = req.body;
    const Id = req.params.empId;
    try{
        await Employee.findByIdAndUpdate(Id,{firstName:firstName,lastName:lastName,email:email,location:location,designation:designation,phone:phone});
        return res.status(200).json({info:`${firstName} ${lastName} Details has been updated`});
    }
    catch(err){
        if(err.kind === 'ObjectId'){
            return res.status(404).json({info:'Employee Details not exists with this ID'});
        }

        return res.status(500).json({error:`Some Error occured while updating Employee - ${err}`});

    }
}

module.exports.deleteEmployee = async(req,res)=>{

    const Id = req.params.empId;
    try{
       const Emp = await Employee.findByIdAndDelete(Id);
       if(!Emp){
        return res.status(404).json({info:'Employee Details not exists in this ID'});
       }
        return res.status(200).json({info:`${Emp.firstName} ${Emp.lastName} Employee Details has been deleted Successfully`});

    }
    catch(err){
        return res.status(500).json({error:`Some error Occured while Deleting Employee Details - ${err}`});
    }

}