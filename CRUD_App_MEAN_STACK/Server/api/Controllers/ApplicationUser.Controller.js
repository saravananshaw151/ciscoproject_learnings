const mongoose = require('mongoose');
require('../Models/ApplicationUser.Model');
const app_User = mongoose.model('Application_User');
const jwt = require('jsonwebtoken');
const config = require('../../config.json');

module.exports.signin = async(req,res)=>{
    const {userEmail,password} = req.body;

    try{
        const userExists = await app_User.findOne({userEmail});
        if(!userExists){
            return res.status(401).json({error:'Invalid User Email'});
        }

        const passwordMatch = (password===userExists.password);
        if(!passwordMatch){
            return res.status(401).json({error:'Incorrect Password'});
        }
        const JWT_token = jwt.sign({email:userEmail,pass:password},config.JWT_Secret);
        res.status(200).json({info:"You have Logged In Successfully",token:JWT_token});

    }
    catch(err){
        return res.status(500).json({error:`Internal server error - ${err}`});
    }

}

module.exports.signup = async(req,res)=>{
    const {userName,userEmail,password,phone} = req.body;
    try{
        const userExist = await app_User.findOne({userEmail});
        if(userExist){
            return res.status(400).json({error:'The Given Email already exists in our System'});
        }

        const newUser = new app_User({userName,userEmail,password,phone});
        await newUser.save();
        const JWT_token = jwt.sign({email:userEmail,contact:phone},config.JWT_Secret,{expiresIn:config.JWT_Expire});
        return res.status(200).json({info:"You have Logged In Successfully",token:JWT_token});

    }
    catch(err){
        res.status(500).json({error:`Internal Server Error - ${err}`});
    }
}

// module.exports.verifyToken = async(req,res)=>{
//     const token = req.body.token;
//     try{
//         jwt.verify(token,config.JWT_Secret,(err,decoded)=>{
//             if(err){
//                 return res.status(400).json({info:'Token verification Failed',boolValue:false});
//             }
//             else{
//                 return res.status(200).json({boolValue:true,decodeValue:decoded});
//             }
//         });

//     }
//     catch(err){
//         return res.status(500).json({error:`Internal server error - ${err}`});
//     }
// }