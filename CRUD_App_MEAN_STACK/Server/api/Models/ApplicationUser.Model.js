const mongoose = require('mongoose');

const ApplicationUser_Schema = new mongoose.Schema({
    userName:{
        type:String,
        required:true
    },
    userEmail:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    }
},
{timestamps:true});

module.exports = mongoose.model("Application_User",ApplicationUser_Schema);