const mongoose = require('mongoose');

const EmployeeSchema = new mongoose.Schema({
    
    firstName:{
        type:String,
        required:true
    },
    lastName: {
        type: String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    designation:{
        type: String,
        required:true
    },
    location:{
        type:String
    },
    phone:{
        type:String,
        required:true
    }
},
{timestamps:true});

module.exports = mongoose.model('Employees_Data',EmployeeSchema);