const express = require('express');
const config = require('./config.json');
const DbConnect = require('./Db.config');
const cors = require('cors');
const Emp_router = require('./api/Routes/Employee.route');
const App_User_router = require('./api/Routes/ApplicationUser.route');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

app.use('/api',Emp_router);
app.use('/api',App_User_router);
DbConnect.connect();
app.listen(config.PORT,()=>{
    console.log(`Server Now Listening on port - ${config.PORT}`);
});

