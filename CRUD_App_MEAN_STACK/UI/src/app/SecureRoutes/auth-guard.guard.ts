import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AppUsersService } from '../services/app-users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(private appUserService:AppUsersService,private router:Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if(this.appUserService.isLoggedIn()!==true){
        window.alert('Access denied due to invalid Token. Please Login Again or contact administrator!');
        this.router.navigate(['user_Validate_Home']);
      }

    return true;
  }
  
}
