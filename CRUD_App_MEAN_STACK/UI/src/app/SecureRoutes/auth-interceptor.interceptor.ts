import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppUsersService } from '../services/app-users.service';

@Injectable()
export class AuthInterceptorInterceptor implements HttpInterceptor {

  constructor(private userService:AppUsersService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const accessToken = this.userService.getAccessToken();
    if(accessToken){
      request = request.clone({
        setHeaders:{
          Authorization:`JWT ${accessToken}`
        }
       });
    }
    return next.handle(request);
  }
}
