import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CRUDService } from '../services/crud.service';
import { NotifyService } from '../notify.service';
import { CustomValidations } from '../custom-validations';


@Component({
  selector: 'app-add-edit-employee',
  templateUrl: './add-edit-employee.component.html',
  styleUrls: ['./add-edit-employee.component.scss']
})
export class AddEditEmployeeComponent implements OnInit {

  dialogForm!:FormGroup;
  AddEditFlag:any;
  selectedOption:string = "";
  phoneNumber:string = "";

  designationsList:string[] = ['Software Engineer','Graduate Engineer Trainee','Test Engineer','Full Stack Engineer','Technical Lead','Project Associate','Technical Consultant','Scrum Master'];

  constructor(private formB:FormBuilder,private CRUDApi:CRUDService,private dialogRef:MatDialogRef<AddEditEmployeeComponent>,@Inject(MAT_DIALOG_DATA) private dialogData:any,private notifier:NotifyService) { }

  ngOnInit(): void {
    this.dialogForm = this.formB.group({
      firstName: this.formB.control('',Validators.compose([Validators.required,Validators.minLength(4),CustomValidations.noSpaceAllowed,CustomValidations.onlyAlphabets])),
      lastName: this.formB.control('',Validators.compose([Validators.required,Validators.minLength(1),CustomValidations.noSpaceAllowed,CustomValidations.onlyAlphabets])),
      email: this.formB.control('',Validators.compose([Validators.required,CustomValidations.emailMatch,CustomValidations.noSpaceAllowed])),
      location:this.formB.control('',Validators.compose([Validators.required,Validators.minLength(3),CustomValidations.noSpaceAllowed,CustomValidations.onlyAlphabets])),
      designation:this.formB.control('',Validators.compose([Validators.required])),
      phone:this.formB.control('',Validators.compose([Validators.required,Validators.minLength(10),Validators.maxLength(10),CustomValidations.noSpaceAllowed,CustomValidations.onlyNumbers]))
    });

    this.AddEditFlag = this.dialogData.AddEdit;
    if(this.AddEditFlag){
      this.dialogForm.controls['firstName'].setValue(this.dialogData.updateObj.firstName);
      this.dialogForm.controls['lastName'].setValue(this.dialogData.updateObj.lastName);
      this.dialogForm.controls['email'].setValue(this.dialogData.updateObj.email);
      this.dialogForm.controls['location'].setValue(this.dialogData.updateObj.location);
      this.dialogForm.controls['designation'].setValue(this.dialogData.updateObj.designation);
      this.dialogForm.controls['phone'].setValue(this.dialogData.updateObj.phone);

      this.selectedOption = this.dialogData.updateObj.designation;
    }
     
    

  }

  Add_Update_Employee(){
   if(this.AddEditFlag===0){
    this.CRUDApi.addEmployee(this.dialogForm.value).subscribe((res:any)=>{
      this.notifier.showNotifications('success',res.info);
      this.dialogRef.close();
     },(err)=>{console.error(err);
      this.notifier.showNotifications('error',err.error.error);
    });
   }
   else{
    this.CRUDApi.updateEmployee(this.dialogData.updateObj._id,this.dialogForm.value).subscribe((res:any)=>{
      this.notifier.showNotifications('success',res.info);
      this.dialogRef.close();
    },(err)=>{
      this.notifier.showNotifications('error',err.error.error);
    });
   }

   

  }

  onSelectChange(value:string){
this.selectedOption = value;
  }

  phoneInput(){
    this.phoneNumber = this.phoneNumber.replace(/[^0-9]/g,'');
    this.phoneNumber = this.phoneNumber.substring(0,10);

  }

}
