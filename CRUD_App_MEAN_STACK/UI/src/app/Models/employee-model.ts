export class EmployeeModel {
    constructor(
        public _id:string,
        public firstName:string,
        public lastName:string,
        public location:string,
        public email:string,
        public designation:string,
        public phone:string
    ){

    }



}
