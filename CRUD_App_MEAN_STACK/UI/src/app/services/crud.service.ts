import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeModel } from '../Models/employee-model';

@Injectable({
  providedIn: 'root'
})
export class CRUDService {

  serverUrl:string = "http://localhost:3000/api";

  constructor(private http:HttpClient) { }


  //get Employees from DB
  getEmployees():Observable<any>{
    return this.http.get<any>(`${this.serverUrl}/getEmployees`,{responseType:'json'});
  }

  addEmployee(emp:EmployeeModel):Observable<object>{
    return this.http.post<EmployeeModel>(`${this.serverUrl}/addEmployee`,emp,{responseType:'json'});
  }

  updateEmployee(Id:string,emp:EmployeeModel):Observable<object>{
    return this.http.put<EmployeeModel>(`${this.serverUrl}/updateEmployee/`+Id,emp,{responseType:'json'});
  }

  deleteEmployee(Id:string):Observable<object>{
    return this.http.delete(`${this.serverUrl}/deleteEmployee/`+Id,{responseType:'json'});
  }

}
