import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import * as CryptoJs from 'crypto-js';
import * as jwtDecode from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppUsersService {

  serverUrl:string = "http://localhost:3000/api";

  private authToken: string | null = null;

  constructor(private http:HttpClient,private router:Router) { }


  signIn(credential:any):Observable<any>{
    return this.http.post<any>(`${this.serverUrl}/login`,credential,{responseType:'json'});

  }

  signUpandLogin(credential:any):Observable<any>{
    return this.http.post<any>(`${this.serverUrl}/signup_Login`,credential,{responseType:'json'});
  }

  passwordEncrypt(pass:string):string{
    return CryptoJs.SHA256(pass).toString();
  }

  setAccessToken(token:string){
    this.authToken = token;
    localStorage.setItem('app-user-token',token);
   }
 
   getAccessToken():string|null{
     return this.authToken || localStorage.getItem('app-user-token');
   }
 
   isLoggedIn():boolean{
     return (localStorage.getItem('app-user-token')!== null)?true:false;
   }
 
   logout(){
     localStorage.removeItem('app-user-token');
     this.authToken = null;
     if(localStorage.getItem('app-user-token')==null){
       this.router.navigate(['user_Validate_Home']);
     }
   }



}
