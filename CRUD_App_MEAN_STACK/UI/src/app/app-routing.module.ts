import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { EmployeeHomePageComponent } from './employee-home-page/employee-home-page.component';
import { AuthGuardGuard } from './SecureRoutes/auth-guard.guard';

const routes: Routes = [
  {path:'',redirectTo:'/user_Validate_Home',pathMatch:'full'},
  {path:'user_Validate_Home',component:LandingPageComponent},
  {path:'Employees_Home',component:EmployeeHomePageComponent,canActivate:[AuthGuardGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
