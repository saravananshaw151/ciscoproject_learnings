import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CRUDService } from '../services/crud.service';
import { NotifyService } from '../notify.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {

  DeleteMessage:string = "Please confirm to Delete the User ";
  UserName:string = "";


  constructor(@Inject(MAT_DIALOG_DATA) private dialogData:any,private CRUDApi:CRUDService,private notify:NotifyService) { }

  ngOnInit(): void {
    this.UserName = `${this.dialogData.DeleteObj.firstName} ${this.dialogData.DeleteObj.lastName}.`;
  }

  deleteConfirm(){
    this.CRUDApi.deleteEmployee(this.dialogData.DeleteObj._id).subscribe((res:any)=>{
      this.notify.showNotifications('success',res.info);
    },(err)=>{
      this.notify.showNotifications('error',err.error.error);
    });

  }
}
