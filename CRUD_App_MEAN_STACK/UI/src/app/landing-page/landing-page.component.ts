import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { NotifyService } from '../notify.service';
import { CustomValidations } from '../custom-validations';
import { AppUsersService } from '../services/app-users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  LoginForm!:FormGroup;
  SignUpForm!:FormGroup;
  SignInSignUp:any = 0; // 0 for login & 1 for signup
  showPassword:boolean = false;
  showConfirmPassword:boolean = false;
  PasswordValue:string = "";

  constructor(private formB:FormBuilder,private notify:NotifyService,private appUser:AppUsersService,private router:Router) { }

  ngOnInit(): void {

    this.LoginForm = this.formB.group({
      userEmail: this.formB.control('',Validators.compose([Validators.required,CustomValidations.noSpaceAllowed,CustomValidations.emailMatch])),
      password:this.formB.control('',Validators.compose([Validators.required,CustomValidations.noSpaceAllowed,CustomValidations.passwordMatch]))
    });

    this.SignUpForm = this.formB.group({
      userName : this.formB.control('',Validators.compose([Validators.required,CustomValidations.noSpaceAllowed])),
      userEmail: this.formB.control('',Validators.compose([Validators.required,CustomValidations.noSpaceAllowed,CustomValidations.emailMatch])),
      password: this.formB.control('',Validators.compose([Validators.required,CustomValidations.passwordMatch,CustomValidations.noSpaceAllowed])),
      confirmPassword : this.formB.control('',Validators.compose([Validators.required,CustomValidations.noSpaceAllowed,CustomValidations.passwordMatch,this.confirmPasswordVerify.bind(this)])),
      phone : this.formB.control('',Validators.compose([Validators.required,Validators.minLength(10)]))
    });

    if(this.appUser.getAccessToken()!==null){
      this.router.navigate(['Employees_Home']);
    }
  }

  public togglePasswordVisibility(){
    this.showPassword = !this.showPassword;
  }
  public toggleConfirmPasswordVisibility(){
    this.showConfirmPassword = !this.showConfirmPassword;
  }

  confirmPasswordVerify(control:AbstractControl):{[key:string]:boolean}|null{ 
    const pass = this.PasswordValue;
      const confirmPass = control.value;
      return (pass===confirmPass)? null : {passwordMismatch:true} ;
  }

  swapLoginSignUp(){
    this.SignInSignUp = (this.SignInSignUp===0)? 1:0;
    this.LoginForm.reset();
    this.SignUpForm.reset();
  }

  onNumberInputChange(event:any){
   event.target.value = event.target.value.replace(/[^0-9]/g,'');
  }

  onPasswordChange(event:any){
    this.PasswordValue = event.target.value;
    console.log('password field value - ',this.PasswordValue);
  }

  loginSubmit(){
    this.LoginForm.value.password = this.appUser.passwordEncrypt(this.LoginForm.value.password);
    this.appUser.signIn(this.LoginForm.value).subscribe((res:any)=>{
      this.notify.showNotifications('info',res.info);
      this.appUser.setAccessToken(res.token);
      this.router.navigate(['Employees_Home']);
      this.LoginForm.reset();
    },(err)=>{
      console.error('error object - ',err);
      this.notify.showNotifications('error',err.error.error);
    }); 

  }

  SignupSubmit(){
    this.SignUpForm.value.password = this.appUser.passwordEncrypt(this.SignUpForm.value.password);
    this.appUser.signUpandLogin(this.SignUpForm.value).subscribe((res)=>{
      this.notify.showNotifications('info',res.info);
      this.appUser.setAccessToken(res.token);
      this.router.navigate(['Employees_Home']);
      this.SignUpForm.reset();
    },(err)=>{
      this.notify.showNotifications('error',err.error.error);
    });

  }


}
