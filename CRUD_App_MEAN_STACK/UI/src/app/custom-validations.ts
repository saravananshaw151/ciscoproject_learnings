import { AbstractControl, ValidationErrors } from "@angular/forms";

export class CustomValidations {

    static noSpaceAllowed(control:AbstractControl):ValidationErrors|null{
        if(control.value &&(control.value as string).includes(" ")){
            return {noSpace:true};
        }
        return null;
    }

    static onlyAlphabets(control:AbstractControl):ValidationErrors|null{
        const regex = /^[a-zA-Z]+$/;
        const NoSpaceValue = (control.value!=null)?control.value.replace(/\s+/g,"") : "";
        if(control.value && !regex.test(NoSpaceValue)){
            return {onlyAlphabets:true};
        }
        return null;
    }

    static onlyNumbers(control:AbstractControl):ValidationErrors|null{
        const regex = /^[0-9]+$/;
        const NoSpaceValue = (control.value!=null)?control.value.replace(/\s+/g,"") : "";
        if(control.value && !regex.test(NoSpaceValue)){
            return {onlyNumbers:true};
        }
        return null;
    }

    static noSpecialCharacters(control:AbstractControl):ValidationErrors|null{
        const regex = /[!@#$%^&*`~()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        if(control.value && regex.test(control.value ) ){
            return {noSpecialChar:true};
        }
        return null;
    }

    static emailMatch(control:AbstractControl) : ValidationErrors|null{
        const emailRegex = /^[a-zA-Z]{5,}[0-9]{3,}@gmail\.com$/;
        const emailNoSpaceValue = (control.value!=null)?control.value.replace(/\s+/g,"") : "";
        if(control.value && !emailRegex.test(emailNoSpaceValue)){
            return {emailVerify:true};
        }
        return null;
    }

    static passwordMatch(control: AbstractControl): ValidationErrors|null{
 
        const characterRegex = /[A-Za-z]/g;
        const digitRegex = /\d/g;
        const specialCharRegex = /[!@#$%^&*`~()_+\-=\[\]{};':"\\|,.<>\/?]+/g;
        const charcheck = (control.value!=null)? characterRegex.test(control.value) && (control.value).match(characterRegex).length>=4 : 0;
        const digitcheck  = (control.value!=null)? digitRegex.test(control.value) && (control.value).match(digitRegex).length>=3 : 0;
        const specialcheck = (control.value!=null)? specialCharRegex.test(control.value) && (control.value).match(specialCharRegex).join('').length>=2 : 0;
       
        // const valid = charcheck && digitcheck && specialcheck;
        if( !charcheck && control.value){
            return {alphabetVerify:true};
        }
        if(!digitcheck && control.value){
            return {numberVerify:true};
        }
        if(!specialcheck && control.value){
            return {specialCharVerify:true};
        }
        return null;
       
    }
}
