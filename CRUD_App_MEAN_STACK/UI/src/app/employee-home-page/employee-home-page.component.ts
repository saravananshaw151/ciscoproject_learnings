import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { CRUDService } from '../services/crud.service';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeModel } from '../Models/employee-model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { AddEditEmployeeComponent } from '../add-edit-employee/add-edit-employee.component';
import { NotifyService } from '../notify.service';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { AppUsersService } from '../services/app-users.service';


@Component({
  selector: 'app-employee-home-page',
  templateUrl: './employee-home-page.component.html',
  styleUrls: ['./employee-home-page.component.scss']
})
export class EmployeeHomePageComponent implements OnInit,AfterViewInit {
 
  displayColumns:string[] = ['firstName','lastName','email','designation','location','phone','actions'];
  dataSource!:MatTableDataSource<EmployeeModel>;
  EmployeesList:EmployeeModel[] = [];


  @ViewChild(MatPaginator) paginator!:MatPaginator;
  @ViewChild(MatSort) sort!:MatSort;


  constructor(private apiService:CRUDService,private dialog:MatDialog,private appUser:AppUsersService) { 
    this.dataSource = new MatTableDataSource<EmployeeModel>();
  }

  ngOnInit(): void {

    this.getEmployeeDetails();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getEmployeeDetails(){
    this.apiService.getEmployees().subscribe((res)=>{
      this.EmployeesList = res;
      this.dataSource.data = res;
      },(err)=>{console.error('error in getting data - ',err)});
  }

  addDialog(){
    const dialogRef = this.dialog.open(AddEditEmployeeComponent,{width:'90vh',height:'75vh',data:{AddEdit:0},disableClose:true,restoreFocus:false,autoFocus:false});
    dialogRef.afterClosed().subscribe((res)=>{
      this.getEmployeeDetails();
    });

  }

  updateDialog(id:string){
  
    const selectedObj = this.EmployeesList.find(obj=>obj._id===id);
    const dialogRef = this.dialog.open(AddEditEmployeeComponent,{width:'90vh',height:'75vh',data:{AddEdit:1,updateObj:selectedObj},disableClose:true,restoreFocus:false,autoFocus:false});
    dialogRef.afterClosed().subscribe(()=>{
      this.getEmployeeDetails();
    });
  }

  deleteDialog(id:string){

    const selectedObj = this.EmployeesList.find(obj=>obj._id===id);

    const dialogRef = this.dialog.open(DeleteDialogComponent,{width:'90vh',height:'40vh',data:{DeleteObj:selectedObj},disableClose:true,restoreFocus:false,autoFocus:false});
    dialogRef.afterClosed().subscribe(()=>{
      this.getEmployeeDetails();
    });

  }

  LogOut(){
    this.appUser.logout();

  }

}
